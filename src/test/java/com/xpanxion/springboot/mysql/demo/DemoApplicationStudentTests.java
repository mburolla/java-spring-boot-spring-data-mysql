package com.xpanxion.springboot.mysql.demo;

import com.xpanxion.springboot.mysql.demo.model.*;
import com.xpanxion.springboot.mysql.demo.repository.CatRepository;
import com.xpanxion.springboot.mysql.demo.repository.Student2Repository;
import com.xpanxion.springboot.mysql.demo.repository.StudentRepository;
import com.xpanxion.springboot.mysql.demo.repository.SubjectRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.List;

import static java.lang.Long.valueOf;

@SpringBootTest
public class DemoApplicationStudentTests {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private Student2Repository student2Repository;


    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private CatRepository catRepository;

    @Test
    public void testStudent() {
        var s = new Student();
        s.setName("Fred");
        s.setLocker(new Locker("Second floor"));
        studentRepository.save(s);
        var l = studentRepository.findByNameContaining("Fre");
        var ss = l.get(0);
        Assert.isTrue(true, "Ok");
    }

    @Test
    public void testStudentOneToMany() {
        // Arrange, Act, Assert.
        var c = new Course("Snacking");
//        var s = new Student();
//        s.setName("Howard");
//        s.setCourses(List.of(c));
//        studentRepository.save(s);

        // Add a course to a student that only has one course.
        var howard = studentRepository.findByNameContaining("Howard").get(0);
        howard.getCourses().add(c);
        studentRepository.save(howard);

        //
        Assert.isTrue(true, "Ok");
    }

    @Test
    public void testStudentManyToMany() {
        var s = new Student("Bill");
        var p = new Pencil(2);
        p.setStudentId(s.getStudentId());
        studentRepository.save(s);
        Assert.isTrue(true, "Ok");
    }

    @Test
    public void testIt() {
//        var student2 = new Student2("Joey");
//        var sub = new Subject("Math");
//        student2.addSubject(sub);
//        student2Repository.save(student2);

        // This works OK
//        var student2 = new Student2("Fred");
//        var subject = new Subject("Gym");
//        subject.addStudent(student2);
//        student2Repository.save(student2);
//        subjectRepository.save(subject);

        // Add subjects to an exsiting student. OK.
//        var student2 = student2Repository.getById(valueOf(1));
//        var subject = new Subject("Science");
//        subject.addStudent(student2);
//        student2Repository.save(student2);
//        subjectRepository.save(subject);

        // Fetch subjects for a student.
        var student2 = student2Repository.findById(valueOf(1));
        System.out.println(student2.get().getSubjects());

        Assert.isTrue(true, "Ok");
    }


}
