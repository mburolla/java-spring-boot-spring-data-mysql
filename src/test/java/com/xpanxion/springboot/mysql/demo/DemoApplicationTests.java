package com.xpanxion.springboot.mysql.demo;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;
import com.xpanxion.springboot.mysql.demo.model.Customer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;
import com.xpanxion.springboot.mysql.demo.repository.ProductRepository;
import com.xpanxion.springboot.mysql.demo.repository.CustomerRepository;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private ProductRepository productRepository;

	@Test
	void getEmailPriceForCustomer() {
		Integer customerId = 22;
		var price = customerRepository.getEmailPrice(customerId).get(0);
		System.out.println(price.getPrice());
		System.out.println(price.getEmail());
		Assert.isTrue(Double.parseDouble(price.getPrice()) > 0.00, "Price should be greater than zero.");
	}

	@Test
	void getProduct() {
		Integer productId = 789;
		var product = productRepository.findById(productId).get();
		Assert.isTrue(product.getName().length() > 0, "Product name is missing.");
	}

	@Test
	void getCustomerName() {
		var customerList = customerRepository.findByName("Marty");
		for(Customer customer : customerList) {
			System.out.println(customer.getId());
			System.out.println(customer.getEmail());
			System.out.println(customer.getName());
		}
		Assert.isTrue(customerList.size() == 2, "Marty is missing.");
	}

	@Test
	void getCustomerNameContaining() {
		var customerList = customerRepository.findByNameContaining("Mar");
		for(Customer customer : customerList) {
			System.out.println(customer.getId());
			System.out.println(customer.getEmail());
			System.out.println(customer.getName());
		}
		Assert.isTrue(customerList.size() == 2, "Names starting with Mar are missing.");
	}
}
