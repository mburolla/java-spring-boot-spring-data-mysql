package com.xpanxion.springboot.mysql.demo.model;

import javax.persistence.*;

@Entity
public class Locker {

    //
    // Data members
    //

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "locker_id", nullable = false)
    private Integer lockerId;

    private String location;

    //
    // Constructors
    //

    public Locker() {}

    public Locker(String location) {
        this.location = location;
    }

    //
    // Accessors
    //

    public Integer getLockerId() {
        return lockerId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setLockerId(Integer lockerId) {
        this.lockerId = lockerId;
    }

}
