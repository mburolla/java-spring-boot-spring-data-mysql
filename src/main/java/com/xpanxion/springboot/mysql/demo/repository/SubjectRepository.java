package com.xpanxion.springboot.mysql.demo.repository;


import com.xpanxion.springboot.mysql.demo.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubjectRepository extends JpaRepository<Subject, Long> {

}
