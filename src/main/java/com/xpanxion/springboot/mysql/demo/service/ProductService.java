package com.xpanxion.springboot.mysql.demo.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.PageRequest;
import com.xpanxion.springboot.mysql.demo.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import com.xpanxion.springboot.mysql.demo.repository.ProductRepository;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductService {

    //
    // Data members
    //

    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    private ProductRepository productRepository;

    //
    // Methods
    //

    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    @Transactional
    public Product getProduct(int id) {
        // Option #1: JPA Repository.
        var p1 = productRepository.findById(id).get(); // findById() returns an Optional.
        p1.setPrice(7.77); // Does not persist to DB.

        // Option #2: Entity Manager.
        var p2 = entityManager.find(Product.class, id);

        //entityManager.detach((p2));
        p2.setPrice(1.11); // Persists to DB when Product entity is in a Managed state, no reason to call persist().

        return p2; // p1
    }

    public List<Product> getAllProducts(Integer pageNumber, Integer pageSize) {
        var pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("name").ascending());
        return productRepository.findAll(pageRequest).getContent();
    }

    public void deleteProduct(int id) {
        productRepository.deleteById(id);
    }

    public Product updateProduct(Product product) {
        // This type of work can also be done in the repository.
        // This approach is more resilient to changes in entity models.
        var retval = productRepository.findById(product.getId()).get(); // findById() returns an Optional.
        retval.setName(product.getName());
        retval.setQuantity(product.getQuantity());
        retval.setPrice(product.getPrice());
        //productRepository.save(retval); // Not necessary because the findByID() method returns an object in the managed/persisted state.
        return retval;
    }
}
