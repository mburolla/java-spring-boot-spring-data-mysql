package com.xpanxion.springboot.mysql.demo.model;

public class OrderResponse {

    //
    // Data members
    //

    private String name;
    private String productName;

    //
    // Constructors
    //

    public OrderResponse() {
    }

    public OrderResponse(String name, String productName) {
        this.name = name;
        this.productName = productName;
    }

    //
    // Accessors
    //

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "OrderResponse{" +
                "name='" + name + '\'' +
                ", productName='" + productName + '\'' +
                '}';
    }
}
