package com.xpanxion.springboot.mysql.demo.model;

public class CustomerName {

    //
    // Data Members
    //

    private String newName;

    //
    // Constructors
    //

    public CustomerName() {
    }

    public CustomerName(String newName) {
        this.newName = newName;
    }

    //
    // Accessors
    //

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "CustomerName{" +
                "newName='" + newName + '\'' +
                '}';
    }
}
