package com.xpanxion.springboot.mysql.demo.repository;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import com.xpanxion.springboot.mysql.demo.model.Price;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Modifying;
import com.xpanxion.springboot.mysql.demo.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import com.xpanxion.springboot.mysql.demo.model.OrderResponse;

@Repository
@Transactional
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    // NATIVE SQL QUERY #1: Positional Arguments
    String emailPriceSql = """
        select email, round(sum(p.price),2) as price
        from customer c
        join product p on c.id = p.customer_id
        where c.id = ?1
    """;

    // NATIVE SQL QUERY #2: Named Parameters
    String updateNameSql = """
        update customer
        set name = :name
        where id = :customerId
    """;

    // JPQL QUERY, based on classes not tables.
    String customerProductNameSql = "select new com.xpanxion.springboot.mysql.demo.model.OrderResponse (c.name, p.name) from Customer c join c.products p";

    @Query(customerProductNameSql)
    List<OrderResponse> getCustomerProductName();

    @Query(value = emailPriceSql, nativeQuery = true)
    List<Price> getEmailPrice(Integer customerId);

    @Modifying
    @Query(value = updateNameSql, nativeQuery = true)
    void updateCustomerName(@Param("name") String newName, @Param("customerId") Integer customerId);

    // JPA Query Method #1
    List<Customer>findByName(String name);

    // JPA Query Method #2
    List<Customer>findByNameContaining(String name);
}
