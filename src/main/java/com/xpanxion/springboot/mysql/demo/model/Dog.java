package com.xpanxion.springboot.mysql.demo.model;

import java.util.Objects;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

@Entity
public class Dog extends Animal implements Serializable {

    //
    // Data members
    //

    @Id
    @GeneratedValue
    private Integer dogId;
    private String favoriteDogFood;

    //
    // Constructors
    //

    public Dog() {
    }

    public Dog(String favoriteDogFood) {
        this.favoriteDogFood = favoriteDogFood;
    }

    //
    // Accessors
    //

    public Integer getCatId() {
        return dogId;
    }

    public void setCatId(Integer catId) {
        this.dogId = catId;
    }

    public String getFavoriteDogFood() {
        return favoriteDogFood;
    }

    public void setFavoriteDogFood(String favoriteDogFood) {
        this.favoriteDogFood = favoriteDogFood;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "Dog{" +
                "dogId=" + dogId +
                ", favoriteDogFood='" + favoriteDogFood + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dog dog = (Dog) o;
        return Objects.equals(dogId, dog.dogId) && Objects.equals(favoriteDogFood, dog.favoriteDogFood);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dogId, favoriteDogFood);
    }
}
