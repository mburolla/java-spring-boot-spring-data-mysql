package com.xpanxion.springboot.mysql.demo.controller;

import java.util.Objects;
import org.springframework.http.HttpStatus;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class SessionIdInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        var sessionId = request.getHeader("x-session-id");
        if (isValidSessionId(sessionId)) {
            response.setHeader("x-session-id", sessionId);
            return true;
        }
        else {
            response.setHeader("x-session-id", sessionId);
            response.getWriter().write("UNAUTHORIZED");
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return false;
        }
    }

    private Boolean isValidSessionId(String sessionId) {
        // TODO: Check db...
        return Objects.equals(sessionId, "123");
    }
}
