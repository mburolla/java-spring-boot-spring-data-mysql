package com.xpanxion.springboot.mysql.demo.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.xpanxion.springboot.mysql.demo.model.Price;
import org.springframework.beans.factory.annotation.Autowired;
import com.xpanxion.springboot.mysql.demo.repository.CustomerRepository;

@Service
public class CustomerService {

    //
    // Data members
    //

    @Autowired
    private CustomerRepository customerRepository;

    //
    // Methods
    //

    public List<Price> getEmailPrice(Integer customerId) {
        return customerRepository.getEmailPrice(customerId);
    }

    public void updateCustomerName(String newName, Integer customerId) {
        customerRepository.updateCustomerName(newName, customerId);
    }
}
