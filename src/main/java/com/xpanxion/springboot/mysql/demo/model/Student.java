package com.xpanxion.springboot.mysql.demo.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Student {

    //
    // Data members
    //

    @Id
    @GeneratedValue
    private Integer studentId;
    private String name;

    @OneToOne(targetEntity = Locker.class, cascade = CascadeType.ALL) // One student has one locker (uni-directional).
    @JoinColumn(name="locker_id", referencedColumnName = "locker_id") // name is the column name in the student table.
    private Locker locker;

    @OneToMany(targetEntity = Course.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER) // One student has many courses.
    @JoinColumn(name="student_id", referencedColumnName = "studentId")
        // name is the column name (DB) in the student table.
        // referencedColumnName is the name of the column (Java) in the course table.
    private List<Course> courses;



    //
    // Constructors
    //

    public Student(){
        // Note: No reason to new up a course list here.
    }

    public Student(String name) {
        this.name = name;
    }

    //
    // Accessors
    //

    public Locker getLocker() {
        return locker;
    }

    public void setLocker(Locker locker) {
        this.locker = locker;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
