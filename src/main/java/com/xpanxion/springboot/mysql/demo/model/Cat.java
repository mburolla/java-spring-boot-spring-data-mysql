package com.xpanxion.springboot.mysql.demo.model;

import java.util.Objects;
import javax.persistence.Id;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

@Entity
public class Cat extends Animal implements Serializable {

    //
    // Data Members
    //

    @Id
    @GeneratedValue
    private Integer catId;
    private String favoriteCatFood;
    private String name;

    //
    // Contructors
    //

    public Cat() {
    }

    public Cat(Integer age, String name, String favoriteCatFood) {
        this.age = age;
        this.name = name;
        this.favoriteCatFood = favoriteCatFood;
    }

    //
    // Accessors
    //

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getFavoriteCatFood() {
        return favoriteCatFood;
    }

    public void setFavoriteCatFood(String favoriteCatFood) {
        this.favoriteCatFood = favoriteCatFood;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "Cat{" +
                "catId=" + catId +
                ", favoriteCatFood='" + favoriteCatFood + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cat cat = (Cat) o;
        return Objects.equals(catId, cat.catId) && Objects.equals(favoriteCatFood, cat.favoriteCatFood);
    }

    @Override
    public int hashCode() {
        return Objects.hash(catId, favoriteCatFood);
    }

    @Override
    public String speak() {
        return "Meow: My name is: " + name;
    }
}
