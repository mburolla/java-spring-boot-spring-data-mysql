package com.xpanxion.springboot.mysql.demo.model;

public class OrderRequest {

    //
    // Data members
    //

    private Customer customer;

    //
    // Constructors
    //

    public OrderRequest() {
        customer = new Customer();
    }

    public OrderRequest(Customer customer) {
        this.customer = customer;
    }

    //
    // Accessors
    //

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "OrderRequest{" +
                "customer=" + customer +
                '}';
    }
}
