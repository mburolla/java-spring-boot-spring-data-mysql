package com.xpanxion.springboot.mysql.demo.repository;

import org.springframework.stereotype.Repository;
import com.xpanxion.springboot.mysql.demo.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> { }
