package com.xpanxion.springboot.mysql.demo.controller;

import java.util.List;
import org.springframework.web.bind.annotation.*;
import com.xpanxion.springboot.mysql.demo.model.Price;
import com.xpanxion.springboot.mysql.demo.model.CustomerName;
import org.springframework.beans.factory.annotation.Autowired;
import com.xpanxion.springboot.mysql.demo.service.CustomerService;

@RestController
public class CustomerController {

    //
    // Data members
    //

    @Autowired
    private CustomerService customerService;

    //
    // GET Methods
    //

    @GetMapping("api/v1/customers/{customerId}/price")
    public List<Price> getEmailPrice(@PathVariable Integer customerId) {
        return customerService.getEmailPrice(customerId);
    }

    //
    // PUT Methods
    //

    @PutMapping("api/v1/customers/{customerId}/name")
    public CustomerName updateCustomerName(@PathVariable Integer customerId, @RequestBody CustomerName customerName) {
        customerService.updateCustomerName(customerName.getNewName(), customerId);
        return customerName;
    }
}
