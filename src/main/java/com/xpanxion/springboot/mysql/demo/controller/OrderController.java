package com.xpanxion.springboot.mysql.demo.controller;

import org.springframework.web.bind.annotation.*;
import com.xpanxion.springboot.mysql.demo.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.xpanxion.springboot.mysql.demo.repository.ProductRepository;
import com.xpanxion.springboot.mysql.demo.repository.CustomerRepository;

import java.util.List;

@RestController
public class OrderController {

    //
    // Data members
    //

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProductRepository productRepository;

    //
    // GET Methods
    //

    @GetMapping("api/v1/orders")
    public List<Customer>getAllOrders() {
        return customerRepository.findAll();
    }

    @GetMapping("api/v1/orders/info")
    public List<OrderResponse>getJoinInformation() {
        return customerRepository.getCustomerProductName();
    }

    //
    // POST Methods
    //

    @PostMapping("api/v1/orders")
    public Customer addOrder(@RequestBody OrderRequest orderRequest) {
        return customerRepository.save(orderRequest.getCustomer());
    }
}
