package com.xpanxion.springboot.mysql.demo.controller;

import org.springframework.web.bind.annotation.*;
import com.xpanxion.springboot.mysql.demo.model.Dog;
import com.xpanxion.springboot.mysql.demo.model.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import com.xpanxion.springboot.mysql.demo.service.AnimalService;

@RestController
public class AnimalController {

    //
    // Data members
    //

    @Autowired
    private AnimalService animalService;

    //
    // POST Methods
    //

    @PostMapping("api/v1/animals/cats")
    public Cat addCat(@RequestBody Cat cat) {
        return animalService.addCat(cat);
    }

    @PostMapping("api/v1/animals/dogs")
    public Dog addDog(@RequestBody Dog dog) {
        return animalService.addDog(dog);
    }
}
