package com.xpanxion.springboot.mysql.demo.util;

import java.util.Date;
import com.nimbusds.jose.*;
import java.text.ParseException;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.jwt.JWTClaimsSet;
import java.nio.charset.StandardCharsets;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;

//<dependency>
//    <groupId>com.nimbusds</groupId>
//    <artifactId>nimbus-jose-jwt</artifactId>
//    <version>9.20</version>
//</dependency>
//
// https://connect2id.com/products/nimbus-jose-jwt/examples/jwt-with-hmac
//

public final class JWTHelper {

    private final static String sharedSecret = "This is my shared secret, it must be at least 256 bits.";

    public static String createJWT(Integer sessionId) throws JOSEException {
        JWSSigner signer = new MACSigner(sharedSecret.getBytes(StandardCharsets.UTF_8));
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject("Shopping Cart")
                .issuer("https://www.xpanxion.com")
                .claim("sessionId", sessionId) // Our main payload.
                .expirationTime(new Date(new Date().getTime() + 60 * 1000))
                .build();
        SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);
        signedJWT.sign(signer);
        return signedJWT.serialize();
    }

    public static Integer readJWT(String jwtString) throws ParseException, JOSEException, Exception {
        var signedJWT = SignedJWT.parse(jwtString);

        // Check for man in the middle attacks.
        signedJWT.verify(new MACVerifier(sharedSecret)); // DO NOT ALTER.

        // Verify token hasn't expired.
        Date now = new Date();
        Date expirationTime = signedJWT.getJWTClaimsSet().getExpirationTime();
        if (expirationTime != null && now.after(expirationTime)) {
            throw new Exception("Token expired");
        }

        var sessionIdOjbect = signedJWT.getJWTClaimsSet().getClaims().get("sessionId");
        return Integer.parseInt(sessionIdOjbect.toString());
    }
}
