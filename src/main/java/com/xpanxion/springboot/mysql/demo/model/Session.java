package com.xpanxion.springboot.mysql.demo.model;

public class Session {
    private String issueDate;
    private Integer sessionId;

    public Session(String issueDate, Integer sessionId) {
        this.issueDate = issueDate;
        this.sessionId = sessionId;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }
}
