package com.xpanxion.springboot.mysql.demo.repository;

import com.xpanxion.springboot.mysql.demo.model.Customer;
import com.xpanxion.springboot.mysql.demo.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Integer> {

    List<Student> findByName(String name);

    List<Student> findByNameContaining(String name);
}
