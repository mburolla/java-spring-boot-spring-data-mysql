package com.xpanxion.springboot.mysql.demo.model;

import java.util.List;
import javax.persistence.*;

@Entity
public class Customer {

    //
    // Data members
    //

    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String email;
    private String gender;

    @OneToMany(targetEntity = Product.class, cascade = CascadeType.ALL) // FetchType = Eager vs Lazy.
    @JoinColumn(name="customer_id", referencedColumnName = "id") // If not specified, hibernate will create a new table.
    private List<Product> products;

    //
    // Constructors
    //

    public Customer() { }

    public Customer(int id, String name, String email, String gender) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    //
    // Accessors
    //

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", productList=" + products +
                '}';
    }
}
