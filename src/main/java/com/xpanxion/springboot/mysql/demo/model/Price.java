package com.xpanxion.springboot.mysql.demo.model;

public interface Price {
    // Our native SQL query returns two columns named: email, price.
    String getEmail();
    String getPrice();
}
