package com.xpanxion.springboot.mysql.demo.controller;

import com.xpanxion.springboot.mysql.demo.model.Session;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@RestController
public class SessionController {

    @GetMapping("api/v1/session")
    public Session getSessionId() {
        var sessionId = 123;  // TODO: Get next session/cart id from db.
        return new Session(Instant.now().toString(), sessionId);
    }
}
