package com.xpanxion.springboot.mysql.demo.controller;

import java.util.List;
import com.xpanxion.springboot.mysql.demo.util.JWTHelper;
import org.springframework.web.bind.annotation.*;
import com.xpanxion.springboot.mysql.demo.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import com.xpanxion.springboot.mysql.demo.service.ProductService;

@RestController
public class ProductController {

    //
    // Data members
    //

    @Autowired
    private ProductService productService;

    //
    // GET Methods
    //

    @GetMapping("api/v1/products/{id}")
    public Product getProduct(@PathVariable int id) {
        jwtTest();
        return productService.getProduct(id);
    }

    @GetMapping("api/v1/products")  // ?page=page&size=size
    public List<Product> getAllProducts(@RequestParam int page, @RequestParam int size) {
        return productService.getAllProducts(page, size);
    }

    //
    // POST Methods
    //

    @PostMapping("api/v1/products")
    public Product addProduct(@RequestBody Product product) {
        return productService.addProduct(product);
    }

    //
    // DEL Methods
    //

    @DeleteMapping("api/v1/products/{id}")
    public String deleteProduct(@PathVariable int id) {
        productService.deleteProduct(id);
        return "OK";
    }

    //
    // PUT Methods
    //

    @PutMapping("api/v1/products/")
    public Product updateProduct(@RequestBody Product product) {
        return productService.updateProduct(product);
    }


    //
    // Helpers
    //

    private void jwtTest() {
        try {
            var jwtString = JWTHelper.createJWT(5150);
            System.out.println(jwtString);
            var sessionId = JWTHelper.readJWT(jwtString);
            System.out.println(sessionId);
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }
}
