package com.xpanxion.springboot.mysql.demo.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name="student_enrolled",
            joinColumns = @JoinColumn(name = "subject_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id")
    )
    private Set<Student2> enrolledStudents = new HashSet<>();

    private String name;

    public Subject() {}

    public Subject(String name) {
        this.name = name;
    }

    public Set<Student2> getEnrolledStudents() {
        return enrolledStudents;
    }

    public void addStudent(Student2 student) {
        enrolledStudents.add(student);
    }

    @Override
    public String toString() {
        return "Subject{" +
                "name='" + name + '\'' +
                '}';
    }
}
