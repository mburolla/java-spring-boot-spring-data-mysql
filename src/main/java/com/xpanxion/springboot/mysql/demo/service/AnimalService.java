package com.xpanxion.springboot.mysql.demo.service;

import javax.transaction.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.xpanxion.springboot.mysql.demo.model.Animal;
import com.xpanxion.springboot.mysql.demo.model.Dog;
import org.springframework.stereotype.Service;
import com.xpanxion.springboot.mysql.demo.model.Cat;

@Service
public class AnimalService {

    //
    // Data members
    //

    @PersistenceContext
    public EntityManager entityManager;

    //
    // Methods
    //

    @Transactional
    public Cat addCat(Cat cat) {
        entityManager.persist(cat);
        cat.setName(cat.speak());
        return cat;
    }

    @Transactional
    public Dog addDog(Dog dog) {
        entityManager.persist(dog);
        return dog;
    }
}
