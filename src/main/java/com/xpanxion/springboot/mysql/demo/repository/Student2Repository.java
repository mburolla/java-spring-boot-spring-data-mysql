package com.xpanxion.springboot.mysql.demo.repository;

import com.xpanxion.springboot.mysql.demo.model.Customer;
import com.xpanxion.springboot.mysql.demo.model.Student2;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface Student2Repository extends JpaRepository<Student2, Long> {

}
