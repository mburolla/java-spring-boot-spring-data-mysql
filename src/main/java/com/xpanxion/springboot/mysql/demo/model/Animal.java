package com.xpanxion.springboot.mysql.demo.model;

import javax.persistence.*;

@MappedSuperclass // Does not create an Animal table.
public abstract class Animal {

    //
    // Data Members
    //

    protected Integer age;

    //
    // Constructors
    //

    public Animal() {
    }

    //
    // Accessors
    //

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    //
    // Methods
    //

    public String speak() {
        return "My age is: " + age;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "Animal{" +
                ", age=" + age +
                '}';
    }
}
