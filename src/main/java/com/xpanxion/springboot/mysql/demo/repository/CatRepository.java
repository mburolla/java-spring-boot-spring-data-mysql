package com.xpanxion.springboot.mysql.demo.repository;

import com.xpanxion.springboot.mysql.demo.model.Cat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CatRepository extends JpaRepository<Cat, Integer> {
}
