package com.xpanxion.springboot.mysql.demo.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Pencil {

    //
    // Data members
    //

    @Id
    @GeneratedValue
    private Integer pencilId;
    private Integer weight;

    private Integer studentId;

    @ManyToMany(targetEntity = Pencil.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name ="student_pencil",
            joinColumns = @JoinColumn(
                    name = "pencil_id",
                    referencedColumnName = "pencilId"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "student_id",
                    referencedColumnName = "studentId"
            )
    )
    private List<Pencil> pencils;

    //
    // Constructors
    //

    public Pencil() {}

    public Pencil(Integer weight) {
        this.weight = weight;
    }

    public Integer getPencilId() {
        return pencilId;
    }

    //
    // Accessors
    //

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public List<Pencil> getPencils() {
        return pencils;
    }

    public void setPencils(List<Pencil> pencils) {
        this.pencils = pencils;
    }

    public void setPencilId(Integer pencilId) {
        this.pencilId = pencilId;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }
}

