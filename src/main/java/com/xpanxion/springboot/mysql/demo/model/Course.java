package com.xpanxion.springboot.mysql.demo.model;

import javax.persistence.*;

@Entity
@Table(name = "course")
public class Course {

    //
    // Private
    //

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "course_id", nullable = false)
    private Integer courseId;
    private String name;

    //
    // Constructors
    //

    public Course() {}

    public Course(String name) {
        this.name = name;
    }

    //
    // Accessors
    //

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer id) {
        this.courseId = id;
    }
}
