# Java Spring Boot Spring Data MySQL
This is a helper project that demonstrates how a Spring Boot API uses a `JpaRepository` to read/write to/from 
a local MySQL database.  The `CustomerRepository` uses native SQL queries and JPQL queries. Functionality such as
error handling, authentication, and validation have been intentionally left out as not to interfere with 
understanding the concepts of the API.

# Architecture
A Spring Boot API is the backend of a modern web application.  The backend is consumed by one
or many frontend technologies (e.g. iPhone, Android, Angular, React).  The diagram below represents the 
internal architecture of the Spring Boot API.  This repository does not contain the frontend.

![](./docs/spring-boot-architecture-opt-1.PNG)

# Entities
- [Product](./src/main/java/com/xpanxion/springboot/mysql/demo/model/Product.java)
- [Customer](./src/main/java/com/xpanxion/springboot/mysql/demo/model/Customer.java)
- [Order](./src/main/java/com/xpanxion/springboot/mysql/demo/model/OrderRequest.java)
- [Dog](./src/main/java/com/xpanxion/springboot/mysql/demo/model/Dog.java)
- [Cat](./src/main/java/com/xpanxion/springboot/mysql/demo/model/Cat.java)

# Endpoints

### GET

GET: localhost:8080/api/v1/products/{id}

GET: localhost:8080/api/v1/customers/{customer id}/price/

### POST

POST: localhost:8080/api/v1/products/
```
{
    "id": 6,
    "name": "Food",
    "quantity": 1,
    "price": 2.22
}
```

POST: localhost:8080/api/v1/orders
```
{
    "customer" : {
        "name": "Marty",
        "email": "mburolla@gmail.com",
        "gender": "male",    
        "products" : [
           {
               "id": 122,
               "name": "mobile",
               "price": 1.34,
               "quantity": 1
           },
           {
               "id": 244,
               "name": "laptop",
               "price": 2.34,
               "quantity": 2
           }
        ]
    }
}
```

POST: localhost:8080/api/v1/animals/cats

```
{
    "age": 4,
    "favoriteCatFood": "Tuna Tuscany",
    "name": "Gypsy"
}
```

POST: localhost:8080/api/v1/animals/dogs
```
{
    "age": 10,
    "favoriteDogFood": "Alpo",
    "name": "Jones"
}
```

### DEL

DEL: localhost:8080/api/v1/products/{id}

### PUT

PUT: localhost:8080/api/v1/products/

```
{
    "id": 6,
    "name": "Food",
    "quantity": 1,
    "price": 2.22
}
```

# Helpful Links
- [Spring Data JPA Reference](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#preface)
- [Single Table](https://youtu.be/IucFDX3RO9U)
- [One-to-Many Relationship](https://youtu.be/8qhaDBCJh6I)
- [JPA Named Query](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.at-query)
- [JPQL](https://youtu.be/2SV7QODVHAE)
- [Spring Data JPA](https://youtu.be/Zyqpo8gxSO0)
- [Spring Data JPA 2](https://youtu.be/ugYyGWoh8fE)
- [Spring Boot Annotations](./annotations.md)
- [Beyond Basics](https://youtu.be/EZwpOLCfuq4)
- [Bindinded Parameters](https://mkyong.com/hibernate/hibernate-parameter-binding-examples/)
- [JPA Query Methods](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation)
- [JPQL](https://www.educba.com/jpql/)
- [Spring Data](https://youtu.be/XszpXoII9Sg)
- [Spring Data JPA vs Hibernate](https://www.stackchief.com/blog/Spring%20Data%20JPA%20vs%20Hibernate)
- [Hibernate Tutorial](https://youtu.be/JR7-EdxDSf0)

# Notes

### Hibernate Lifecyle States
- Transient
- Persisted/Managed
- Detached
- Removed

[YouTube](https://youtu.be/Y7PpjerZkc0)

# Data Access Abstraction Layers
JPA is a specification (interface) implemented with Hibernate.
- High abstraction: Spring Data JPA 
  - Contains `JPARepository` which wraps the `EntityManager`
- Medium: JPA 
  - Contains the `EntityManager`
  - Implemented with Hibernate
- Low: Spring JDBC
  - `JdbcTemplate`
- Really Low: JDBC

#### Application.Properties File
```
spring.datasource.driver-class-name = com.mysql.cj.jdbc.Driver
spring.datasource.url = jdbc:mysql://localhost:3306/marty
spring.datasource.username = root
spring.datasource.password = xxxx
spring.jpa.hibernate.ddl-auto = update
spring.jpa.show-sql = true
spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5Dialect
spring.jpa.properties.format_sql = true
```

#### JPA vs Hibernate
[JPA vs Hibernate](https://youtu.be/SH29O-bcQlc)
- `EntityManager` requires `persistence.xml`
```
<properties>
    <property name="javax.persistence.jdbc.driver" value="com.mysql.cj.jdbc.Driver" />
    <property name="javax.persistence.jdbc.url" value="jdbc:mysql://localhost/musicdb" />
    <property name="javax.persistence.jdbc.user" value="root" />
    <property name="javax.persistence.jdbc.password" value="" />
    <property name="hibernate.dialect" value="org.hibernate.dialect.MySQL8Dialect" />
    <property name="hibernate.show_sql" value="true" />
    <property name="hibernate.format_sql" value="true" />
    <property name="hibernate.hbm2ddl.auto" value="update" />
</properties>
```
- Need to create `EntityManagerFactory`, to create `EntityManager`
- Have to `getTransaction().begin();` and `getTransaction.commit();`

```
EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
EntityManager manager = factory.createEntityManager();
```

# Misc
Built with IntelliJ IDEA 2021.2.2 (Community Edition)
